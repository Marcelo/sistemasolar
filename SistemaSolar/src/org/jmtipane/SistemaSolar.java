package org.jmtipane;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;



/**
 * Taller_4_Sistema_Solar
 * @author marce
 * Programa que dibuja el sistema solar
 * en el que aparecen estrellas de forma aleatoria
 * Los planetas orbitan al sol deforma, horaria y antihoraria
 */
public class SistemaSolar implements GLEventListener {

    //variables de Opengl
    static GL gl;
    static GLU glu;
    static GLUT glut;
    //variables para guardar posición y ángulo
    static float ang=0, pos=600, x1,x2;
    //vector para almacenar estrellas
    static float star[][]= new float[100][2];
    /**
     * Crea una ventana  y la muestra
     * @param args 
     */
    public static void main(String[] args) {
        Frame frame = new Frame("Sistema Solar");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new SistemaSolar());
        frame.add(canvas);
        frame.setSize(1000, 1000);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    /**
     * Inicializa variables y color
     * @param drawable 
     */
    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(5);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    /**
     * Define la forma, perspectiva y dimensiones de la pantalla
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
//        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glOrtho(-300, 300, -300, 300, 300, -300);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Instancia miembros de clase figuraGeométrica y planetas, para dibujarlos
     * Dibuja estrellas de forma aleatoria sobre toda la pantalla
     * @param drawable 
     */
    public void display(GLAutoDrawable drawable) {
        gl = drawable.getGL();
        glu = new GLU();
        glut = new GLUT();
        //actualiza el ángulo de rotación para cada cuerpo
        ang=ang+0.5f;
        
        //permite usar profundidad
        gl.glEnable(GL.GL_DEPTH_TEST);
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        
        //Generador de estrellas 2
        FigurasGeometricas figura = new FigurasGeometricas();
        if(ang%4 == 0 || ang <1){
            for (int i = 0; i < 100; i++) {
                x1=(float) Math.random()*600-300;
                x2=(float) Math.random()*600-300;
                star[i][0]=x1;
                star[i][1]=x2;
                figura.Estrella(x1, x2, -10);
            }
        }
        else{
            for (int i = 0; i < 100; i++) {
                x1=star[i][0];
                x2=star[i][1];
                figura.Estrella(x1, x2, -10);
            }
        }
            
        //Generador de estrellas 1
//        for (int i = 0; i < 15; i++) {
//                x1=(float) Math.random()*600-300;
//                x2=(float) Math.random()*600-300;
//                figura.Estrella(x1, x2, 0);
//        }
        
        //Define la posición del cometa para que vuelva a aparecer
        if(pos<-300)
            pos=6000;
        else
            pos-=20f;
        //Dibuja al cometa/estrella fugaz
        figura.Cometa(pos, pos, 80, 5, ang);
        
        //Rota y traslada todo el sistema
        gl.glPushMatrix();
        gl.glTranslatef(0, 25, 0);
        gl.glRotatef(90, 0.5f, 0.5f, -0.6f);
        //Instancia un planeta, y dibuja el sol y los planetas del sistema solar
            Planetas planeta = new Planetas();
            figura.Sol(0, 0, 0, 40, ang*5);
            planeta.Mercurio(ang+150);
            planeta.Venus(-ang-80);
            planeta.Tierra(ang+80);
            planeta.Marte(ang*1.5f);
            planeta.Jupiter((ang/2)-20);
            planeta.Saturno(ang+50);
            planeta.Urano(-ang);
            planeta.Neptuno(-(ang/2)+60);
        gl.glPopMatrix();
        
        
        
        // Flush all drawing operations to the graphics card
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

