/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jmtipane;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import static javax.media.opengl.GL.GL_BLEND;
import static javax.media.opengl.GL.GL_ONE_MINUS_SRC_ALPHA;
import static javax.media.opengl.GL.GL_SRC_ALPHA;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import static org.jmtipane.SistemaSolar.gl;
import static org.jmtipane.SistemaSolar.glu;
import static org.jmtipane.SistemaSolar.glut;

/**
 * Taller_4_Sistema_Solar
 * @author Marcelo Tip�n
 * Fecha: 01/08/2020
 * Clase que contiene figuras geom�tricas
 */
public class FigurasGeometricas {
    
    //Forma quadric, para dibujar
    GLUquadric quad;
    
    /**
     * Constructor que incializa formas para dibujarlas
     */
    public FigurasGeometricas(){
        quad = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
        glut = new GLUT();
    }
    
    /**
     * Crea una circunferencia 
     * @param posx
     * @param posy
     * @param radio 
     */
    public void Circulo(float posx, float posy, float radio){
        float x,y;
        gl.glBegin(GL.GL_POLYGON);
        for(float i=0; i<10;i+=0.01){
                x=(float) (radio *Math.cos(i));
                y=(float) (radio *Math.sin(i));
                gl.glVertex3f(x+posx, y+posy, 0.0f);
            }
        gl.glEnd();
    }
    
    /**
     * Dibuja una linea circular mediante puntos
     * @param posx
     * @param posy
     * @param radio 
     */
    public void CirculoL(float posx, float posy, float radio){
        float x,y;
        gl.glBegin(GL.GL_POINTS);
        for(float i=0; i<10;i+=0.1){
                x=(float) (radio *Math.cos(i));
                y=(float) (radio *Math.sin(i));
                gl.glVertex3f(x+posx, y+posy, 0);
            }
        gl.glEnd();
    }
    
    /**
     * Dibuja el sol a partir de un radio y un �ngulo de rotaci�n
     * para rotarlo
     * @param x
     * @param y
     * @param z
     * @param radio
     * @param ang 
     */
    public void Sol(float x, float y, float z, float radio, float ang){
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 0);
        gl.glTranslatef(x, y, z);
        gl.glRotated(ang, 0, 0, 1);
        glu.gluSphere(quad, radio, 50, 50);
        
        //Dibuja puntos alrededor del sol
        float x1,x2,x3;
        for (int i = 0; i < 10; i+=1f) {
            for (int j = 0; j < 10; j+=1f) {
                gl.glPushMatrix();
                gl.glColor3f(0.0f, 0.0f, 0);
                gl.glBegin(GL.GL_POINTS);
                x1=(float) ((radio) *Math.cos(j));
                x2=(float) ((radio) *Math.sin(j)*Math.sin(i));
                x3=(float) ((radio) *Math.sin(j)*Math.cos(i));
                gl.glVertex3f(x1, x2, x3);
                gl.glEnd();
            }
        }
        gl.glPopMatrix();
        
    }
    
    /**
     * Dibuja un planeta en posicion xyz, con un radio y �ngulo de giro,
     * para rotarlo y trasladarlo
     * @param x
     * @param y
     * @param z
     * @param radio
     * @param ang 
     */
    public void Planeta(float x, float y, float z, float radio, float ang){
        gl.glPushMatrix();
        gl.glRotatef(ang, 0.0f, 0.0f, 1.0f);
        gl.glTranslatef(x, y, z);
        gl.glRotatef(ang, 0, 0, 1);
        glu.gluSphere(quad, radio, 20, 20);
        gl.glPopMatrix();
    }
    /**
     * Dibuja lunas alrededor de un planeta, con el mismo �ngulo de giro del 
     * planeta para rotar y trasladar
     * y un �ngulo propio para rotar y trasladar
     * @param x
     * @param y
     * @param z
     * @param d
     * @param radio
     * @param ang
     * @param ang1 
     */
    public void Luna(float x, float y, float z, float d, float radio, float ang, float ang1){
        gl.glPushMatrix();
        gl.glRotatef(ang, 0.0f, 0.0f, 1.0f);
        gl.glTranslatef(x, y, z);
        
        gl.glRotatef(ang1*1.5f, 0.0f, 0, 1);
        gl.glTranslatef(d, y, z);

        gl.glColor3f(0.5f, 0.4f, 0.5f);
        glu.gluSphere(quad, radio, 10, 10);
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja las orbitas de los planetas alrededor del sol con puntos
     * @param radio
     * @param ang 
     */
    public void Orbita(float radio, float ang){
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 1);
        gl.glRotatef(ang, 0, 0, 1.0f);
        CirculoL(0, 0, radio);
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja los anillos de saturno a partir de tres discos
     * @param x
     * @param y
     * @param z
     * @param ang 
     */
    public void Anillo(float x, float y, float z, float ang){
        gl.glPushMatrix();
        gl.glRotated(ang, 0, 0, 1);
        gl.glTranslatef(x, y, z);
        gl.glRotated(ang, -0.3f, -0.2f, 1);
        
        gl.glColor3f(1, 0, 0.2f);
        glu.gluDisk(quad, 15, 17, 20, 20);
        gl.glColor3f(1, 0.0f, 0);
        glu.gluDisk(quad, 17.5f, 18.5f, 20, 20);
        gl.glColor3f(1, 0.0f, 0.5f);
        glu.gluDisk(quad, 19, 21, 20, 20);
        
        gl.glPopMatrix();
    }
    
    /**
     * Dibuja un cometa a partir de una esfera y un tri�ngulo 
     * @param x
     * @param y
     * @param z
     * @param radio
     * @param ang 
     */
    public void Cometa(float x, float y, float z, float radio, float ang){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(180, 1.0f, 1, 1);
        gl.glColor3f(1, 1, 0);
        glu.gluSphere(quad, radio, 50, 50);
        glut.glutSolidCone(radio, 100, 100, 30);
        gl.glPopMatrix();
    }
    /**
     * Dibuja una estrella a partir de dos l�neas, un c�rculo y transparencias
     * @param x
     * @param y
     * @param z 
     */
    public void Estrella(float x, float y, float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
            gl.glEnable(GL_BLEND);
            gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            gl.glColor4f(1, 1, (float) Math.random(), 0.2f);
            gl.glBegin(GL.GL_LINES);
            gl.glVertex3f(0, 3, 0);
            gl.glVertex3f(0, -3, 0);
            gl.glVertex3f(3, 0, 0);
            gl.glVertex3f(-3, 0, 0);
            gl.glEnd();
            
            glu.gluSphere(quad, 1.5f, 10, 10);
//            Circulo(x, y, -2);
            
        gl.glPopMatrix();
    }
}
