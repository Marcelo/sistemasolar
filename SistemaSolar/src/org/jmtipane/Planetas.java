
package org.jmtipane;

import static org.jmtipane.SistemaSolar.gl;

/**
 * Taller_4_Sistema_Solar
 * @author Marcelo Tip�n
 * Fecha: 01/08/2020
 * Clase que contiene a los planetas del sistema solar
 */
public class Planetas {
    
    /**
     * Constructor para iniciar planetas
     */
    public Planetas(){
    }
    
    //instancia un miembro de la clase figuraGeometrica para dibujar
    //las esferas 
    FigurasGeometricas figura = new FigurasGeometricas();
    
    //Crea a los planetas, y los hace rotar a partir de un �ngulo
    //Define el color, posici�n, radio y n�mero de lunas
    public void Mercurio(float ang){
        gl.glColor3f(0.5f, 0.5f, 0.4f);
        figura.Planeta(60, 0, 0, 5, ang);
        figura.Orbita(60, ang);
    }
    public void Venus(float ang){
        gl.glColor3f(0.7f, 1.0f, 0.8f);
        figura.Planeta(80, 0, 0, 6, ang);
        figura.Orbita(80, ang);
    }
    public void Tierra(float ang){
        gl.glColor3f(0, 0.6f, 1.0f);
        figura.Planeta(110, 0, 0, 8, ang);
        figura.Orbita(110, ang);
        figura.Luna(110, 0, 0, 12,2, ang, ang*10);
    }
    public void Marte(float ang){
        gl.glColor3f(1, 0, 0);
        figura.Planeta(140, 0, 0, 6, ang);
        figura.Orbita(140, ang);
        figura.Luna(140, 5, 0, 7, 2, ang, ang*6);
        figura.Luna(140, -2, 1, 8,1, ang, -ang*10);
    }
    public void Jupiter(float ang){
        gl.glColor3f(1, 0.5f, 0);
        figura.Planeta(180, 0, 0, 20, ang);
        figura.Orbita(180, ang);
        figura.Luna(180, 5, 0, 29, 3, ang, ang*6);
        figura.Luna(180, -5, 1, 27, 2, ang, -ang*2);
        figura.Luna(180, 0, 2, 25, 1, ang, ang*10);
    }
    public void Saturno(float ang){
        gl.glColor3f(1, 1, 0.5f);
        figura.Planeta(240, 0, 0, 14, ang);
        figura.Orbita(240, ang);
        figura.Anillo(240, 0, 0 ,ang);
    }
    public void Urano(float ang){
        gl.glColor3f(0.0f, 0.5f, 0.5f);
        figura.Planeta(280, 0, 0, 6, ang);
        figura.Orbita(280, ang);
    }
    public void Neptuno(float ang){
        gl.glColor3f(0.0f, 0.1f, 1.0f);
        figura.Planeta(300, 0, 0, 4, ang);
        figura.Orbita(300, ang);
    }
}
