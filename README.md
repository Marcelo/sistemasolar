# Sistemasolar

Este programa hace uso de librerías OpenGl para dibujar el sistema solar, además de implementar movimientos a los planetas.

Este proyecto contiene 2 carpetas: 

1. La primera carpeta contiene imágenes referenciales del programa.
    ![imagen1](https://framagit.org/Marcelo/sistemasolar/-/raw/master/Im%C3%A1genes/SistemaSolar.png)
2. La segunda carpeta contiene los archivos correspondientes al programa:
    

- build
- nbproject
- src/org/jmtipane
- build.xml
- manifest.mf